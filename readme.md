# Roman Catholic calendars

This repository contains various calendar propers for general and particular calendars of various General Roman Calendar revisions.

We do our best to add citations for every celebration and every note, however, we surely have some uncited data. Every uncited data is marked with <sup>[[_citation needed_](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed)]</sup> (as is used in Wikipedia).

## Calendar inheritance

Roman Calendar (at least its lastest, 1969 revision) uses inheritance of celebrations, i.e. celebrations listed in more general calendars are inherited in more specific calendars. This also implies that no celebration inscribed in more general calendar should be listed in more specific calendars which inherit from the more general calendar _unless the more specific calendar changes a particular celebration_.

We call the levels of calendar inheritance _a calendars type_. Here is a list of calendar types ordered from more general calendars to more specific calendars:

- `general` for the General Roman Calendar:
   - all calendars inherit from this calendar;
- `continental` for continental particular calendars:
   - note that these are not actually used, however, other calendars still inherit them;
   - these calendars contain mostly patrons of continents;
   - these calendars inherit from `general` only;
- `national` for national particular calendars:
   - these calendars inherit both from `general` and `continental`;
- `diocesan` for diocesan particular calendars:
   - these calendars inherit both from `general`, `continental` and `national`;
   - some countries don’t use this type intensively (e.g. Slovakia), however, they still include some changes in comparison with `national` (e.g. diocesan patrons);
- `parish` for parish particular calendars:
   - these calendars inherit both from `general`, `continental`, `national` and `diocesan`;
   - these calendars include some changes in comparison with `national` (e.g. diocesan patrons);
   - note that although including these calendars is welcome, for now, we won’t try and collect all parish calendars used all over world (for obvious reasons).
- `religious` for institutional and societal particular calendars:
   - I _think_ these inherit from parish calendars where a particular community/monastery/cloister lives, however, I presume there a special rules how to ‘merge’ the calendars in case of conflicts;
- `personal` for personal jurisdiction particular calendars:
   - I _think_ these inherit from either `national` or `continental` calendars.

## Repository structure

Folder structure is like this: `${cal_revision}/${cal_type}/${cal_name}.md`, where:

- `${cal_revision}` is the year when a particular calendar revision was made (e.g. `1969`);
- `${cal_type}` is one of the following:
   - `general` for the General Roman Calendar;
   - `continental` for continental particular calendars;
   - `national` for national particular calendars;
   - `diocesan` for diocesan particular calendars;
   - `religious` for institutional and societal particular calendars;
   - `personal` for personal jurisdiction particular calendars;
   - `parish` for parish particular calendars;
- `${cal_name}` is name of particular calendar:
   - for `general` it is always `general`;
   - for `continental` it is continent name (e.g. `europe`);
   - for `national` it is continent and country name separated with a dot (e.g. `europe.slovakia`) or group of countries in alphabetic order (e.g. `europe.austria_germany_switzerland`);
   - for `diocesan` it is continent, country and diocese name (e.g. `europe.slovakia.bratislava`);
   - for `religious` it is institute/society/order short name in singular (e.g. `dominican`);
   - for `personal` it is personal jurisdiction name (e.g. `chair_of_st_peter`);
   - for `parish` it is continent, country, diocese and parish name (e.g. `europe.slovakia.banska_bystrica.foncorda`).

:exclamation: Note that only those folders exist which contain at least one calendar file.

## Calendar file contents

Each calendar revision can modify the following structure a bit, however, it should comply as much as possible to this structure.

Every calendar should have:

1. calendar name as heading 1 (e.g. `# General Roman Calendar`);
2. after calendar name, there should be notes on what are the main sources for the calendar:
   - no celebration cited from those sources should be referenced individually;
   - every celebration cited from other sourced should have a reference to the source (otherwise it requires <sup>[[_citation needed_](https://en.wikipedia.org/wiki/Wikipedia:Citation_needed)]</sup>);
   - for all calendars but `general`, we need to list all calendars from which the calendar inherits celebrations with links to them;
3. every month should be formatted as heading 2 (e.g. `## January`);
4. every month should contain a table with the following columns:
   - `Date`: celebration date;
   - `Name`: celebration name;
   - `Rank`: celebration rank (according to calendar revision);
   - `Change type`: change in comparison with the previous version of the calendar or the calendar(s) from which the calendar inherited;
   - `Notes`: notes on changes or special liturgical rules;
5. list of references as heading 2 (`## References`):
   - this list should contain all references used in calendar.